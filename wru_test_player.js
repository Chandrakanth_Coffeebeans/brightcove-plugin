// import { generateSignature } from "./GenerateSign.js";


/*
This method accepts the URL, Content type and Secret Key in order to generate an encoded signature.
The signature and the date used are returned to the user.
The sample parameter values are
 URL: http://host:port/some/api
 ContentType: application/json,
 secret: xxxxxxxxxxxxx  [shared secret key for the application]

 NOTE: application/JSON & application/json are different.
       URL to be sent in string format
*/
function generateSignature(urlString, secret, contentType) {
  var url = document.createElement("a");
  url.href = urlString;
  var uri = url.pathname;
  //console.log("URI: " + uri);
  if (uri === "") {
    uri = "/";
  }
  var queryString = new URLSearchParams(url.search.substring(1));
  //console.log("queryString: " + queryString);
  if ((queryString.toString()).length > 0) {     //queryString !== ""
    uri = uri + "?" + queryString;
  }
  var date = new Date().toUTCString();
  //var date = 'Tue, 19 Jun 2018 06:10:08 GMT';
  var canonicalString = contentType.concat(",", uri, ",", date);
  //console.log(canonicalString);
  var signature = CryptoJS.enc.Base64.stringify(
    CryptoJS.HmacSHA1(canonicalString, secret)
  );
  return [signature, date];
}

var CryptoJS;
CryptoJS =
  CryptoJS ||
  (function(g, l) {
    var e = {},
      d = (e.lib = {}),
      m = function() {},
      k = (d.Base = {
        extend: function(a) {
          m.prototype = this;
          var c = new m();
          a && c.mixIn(a);
          c.hasOwnProperty("init") ||
            (c.init = function() {
              c.$super.init.apply(this, arguments);
            });
          c.init.prototype = c;
          c.$super = this;
          return c;
        },
        create: function() {
          var a = this.extend();
          a.init.apply(a, arguments);
          return a;
        },
        init: function() {},
        mixIn: function(a) {
          for (var c in a) a.hasOwnProperty(c) && (this[c] = a[c]);
          a.hasOwnProperty("toString") && (this.toString = a.toString);
        },
        clone: function() {
          return this.init.prototype.extend(this);
        }
      }),
      p = (d.WordArray = k.extend({
        init: function(a, c) {
          a = this.words = a || [];
          this.sigBytes = c !== l ? c : 4 * a.length;
        },
        toString: function(a) {
          return (a || n).stringify(this);
        },
        concat: function(a) {
          var c = this.words,
            q = a.words,
            f = this.sigBytes;
          a = a.sigBytes;
          this.clamp();
          if (f % 4)
            for (var b = 0; b < a; b++)
              c[(f + b) >>> 2] |=
                ((q[b >>> 2] >>> (24 - 8 * (b % 4))) & 255) <<
                (24 - 8 * ((f + b) % 4));
          else if (65535 < q.length)
            for (b = 0; b < a; b += 4) c[(f + b) >>> 2] = q[b >>> 2];
          else c.push.apply(c, q);
          this.sigBytes += a;
          return this;
        },
        clamp: function() {
          var a = this.words,
            c = this.sigBytes;
          a[c >>> 2] &= 4294967295 << (32 - 8 * (c % 4));
          a.length = g.ceil(c / 4);
        },
        clone: function() {
          var a = k.clone.call(this);
          a.words = this.words.slice(0);
          return a;
        },
        random: function(a) {
          for (var c = [], b = 0; b < a; b += 4)
            c.push((4294967296 * g.random()) | 0);
          return new p.init(c, a);
        }
      })),
      b = (e.enc = {}),
      n = (b.Hex = {
        stringify: function(a) {
          var c = a.words;
          a = a.sigBytes;
          for (var b = [], f = 0; f < a; f++) {
            var d = (c[f >>> 2] >>> (24 - 8 * (f % 4))) & 255;
            b.push((d >>> 4).toString(16));
            b.push((d & 15).toString(16));
          }
          return b.join("");
        },
        parse: function(a) {
          for (var c = a.length, b = [], f = 0; f < c; f += 2)
            b[f >>> 3] |= parseInt(a.substr(f, 2), 16) << (24 - 4 * (f % 8));
          return new p.init(b, c / 2);
        }
      }),
      j = (b.Latin1 = {
        stringify: function(a) {
          var c = a.words;
          a = a.sigBytes;
          for (var b = [], f = 0; f < a; f++)
            b.push(
              String.fromCharCode((c[f >>> 2] >>> (24 - 8 * (f % 4))) & 255)
            );
          return b.join("");
        },
        parse: function(a) {
          for (var c = a.length, b = [], f = 0; f < c; f++)
            b[f >>> 2] |= (a.charCodeAt(f) & 255) << (24 - 8 * (f % 4));
          return new p.init(b, c);
        }
      }),
      h = (b.Utf8 = {
        stringify: function(a) {
          try {
            return decodeURIComponent(escape(j.stringify(a)));
          } catch (c) {
            throw Error("Malformed UTF-8 data");
          }
        },
        parse: function(a) {
          return j.parse(unescape(encodeURIComponent(a)));
        }
      }),
      r = (d.BufferedBlockAlgorithm = k.extend({
        reset: function() {
          this._data = new p.init();
          this._nDataBytes = 0;
        },
        _append: function(a) {
          "string" === typeof a && (a = h.parse(a));
          this._data.concat(a);
          this._nDataBytes += a.sigBytes;
        },
        _process: function(a) {
          var c = this._data,
            b = c.words,
            f = c.sigBytes,
            d = this.blockSize,
            e = f / (4 * d);
          e = a ? g.ceil(e) : g.max((e | 0) - this._minBufferSize, 0);
          a = e * d;
          f = g.min(4 * a, f);
          if (a) {
            for (var k = 0; k < a; k += d) this._doProcessBlock(b, k);
            k = b.splice(0, a);
            c.sigBytes -= f;
          }
          return new p.init(k, f);
        },
        clone: function() {
          var a = k.clone.call(this);
          a._data = this._data.clone();
          return a;
        },
        _minBufferSize: 0
      }));
    d.Hasher = r.extend({
      cfg: k.extend(),
      init: function(a) {
        this.cfg = this.cfg.extend(a);
        this.reset();
      },
      reset: function() {
        r.reset.call(this);
        this._doReset();
      },
      update: function(a) {
        this._append(a);
        this._process();
        return this;
      },
      finalize: function(a) {
        a && this._append(a);
        return this._doFinalize();
      },
      blockSize: 16,
      _createHelper: function(a) {
        return function(b, d) {
          return new a.init(d).finalize(b);
        };
      },
      _createHmacHelper: function(a) {
        return function(b, d) {
          return new s.HMAC.init(a, d).finalize(b);
        };
      }
    });
    var s = (e.algo = {});
    return e;
  })(Math);

(function() {
  var g = CryptoJS,
    l = g.lib,
    e = l.WordArray,
    d = l.Hasher,
    m = [];
  l = g.algo.SHA1 = d.extend({
    _doReset: function() {
      this._hash = new e.init([
        1732584193,
        4023233417,
        2562383102,
        271733878,
        3285377520
      ]);
    },
    _doProcessBlock: function(d, e) {
      for (
        var b = this._hash.words,
          n = b[0],
          j = b[1],
          h = b[2],
          g = b[3],
          l = b[4],
          a = 0;
        80 > a;
        a++
      ) {
        if (16 > a) m[a] = d[e + a] | 0;
        else {
          var c = m[a - 3] ^ m[a - 8] ^ m[a - 14] ^ m[a - 16];
          m[a] = (c << 1) | (c >>> 31);
        }
        c = ((n << 5) | (n >>> 27)) + l + m[a];
        c =
          20 > a
            ? c + (((j & h) | (~j & g)) + 1518500249)
            : 40 > a
            ? c + ((j ^ h ^ g) + 1859775393)
            : 60 > a
            ? c + (((j & h) | (j & g) | (h & g)) - 1894007588)
            : c + ((j ^ h ^ g) - 899497514);
        l = g;
        g = h;
        h = (j << 30) | (j >>> 2);
        j = n;
        n = c;
      }
      b[0] = (b[0] + n) | 0;
      b[1] = (b[1] + j) | 0;
      b[2] = (b[2] + h) | 0;
      b[3] = (b[3] + g) | 0;
      b[4] = (b[4] + l) | 0;
    },
    _doFinalize: function() {
      var d = this._data,
        e = d.words,
        b = 8 * this._nDataBytes,
        g = 8 * d.sigBytes;
      e[g >>> 5] |= 128 << (24 - (g % 32));
      e[(((g + 64) >>> 9) << 4) + 14] = Math.floor(b / 4294967296);
      e[(((g + 64) >>> 9) << 4) + 15] = b;
      d.sigBytes = 4 * e.length;
      this._process();
      return this._hash;
    },
    clone: function() {
      var e = d.clone.call(this);
      e._hash = this._hash.clone();
      return e;
    }
  });
  g.SHA1 = d._createHelper(l);
  g.HmacSHA1 = d._createHmacHelper(l);
})();
(function() {
  var g = CryptoJS,
    l = g.enc.Utf8;
  g.algo.HMAC = g.lib.Base.extend({
    init: function(e, d) {
      e = this._hasher = new e.init();
      "string" === typeof d && (d = l.parse(d));
      var g = e.blockSize,
        k = 4 * g;
      d.sigBytes > k && (d = e.finalize(d));
      d.clamp();
      for (
        var p = (this._oKey = d.clone()),
          b = (this._iKey = d.clone()),
          n = p.words,
          j = b.words,
          h = 0;
        h < g;
        h++
      ) {
        n[h] ^= 1549556828;
        j[h] ^= 909522486;
      }
      p.sigBytes = b.sigBytes = k;
      this.reset();
    },
    reset: function() {
      var e = this._hasher;
      e.reset();
      e.update(this._iKey);
    },
    update: function(e) {
      this._hasher.update(e);
      return this;
    },
    finalize: function(e) {
      var d = this._hasher;
      e = d.finalize(e);
      d.reset();
      return d.finalize(this._oKey.clone().concat(e));
    }
  });
})();

(function() {
  // Shortcuts
  var C = CryptoJS;
  var C_lib = C.lib;
  var WordArray = C_lib.WordArray;
  var C_enc = C.enc;

  /**
   * Base64 encoding strategy.
   */
  var Base64 = (C_enc.Base64 = {
    /**
     * Converts a word array to a Base64 string.
     *
     * @param {WordArray} wordArray The word array.
     *
     * @return {string} The Base64 string.
     *
     * @static
     *
     * @example
     *
     *     var base64String = CryptoJS.enc.Base64.stringify(wordArray);
     */
    stringify: function(wordArray) {
      // Shortcuts
      var words = wordArray.words;
      var sigBytes = wordArray.sigBytes;
      var map = this._map;

      // Clamp excess bits
      wordArray.clamp();

      // Convert
      var base64Chars = [];
      for (var i = 0; i < sigBytes; i += 3) {
        var byte1 = (words[i >>> 2] >>> (24 - (i % 4) * 8)) & 0xff;
        var byte2 = (words[(i + 1) >>> 2] >>> (24 - ((i + 1) % 4) * 8)) & 0xff;
        var byte3 = (words[(i + 2) >>> 2] >>> (24 - ((i + 2) % 4) * 8)) & 0xff;

        var triplet = (byte1 << 16) | (byte2 << 8) | byte3;

        for (var j = 0; j < 4 && i + j * 0.75 < sigBytes; j++) {
          base64Chars.push(map.charAt((triplet >>> (6 * (3 - j))) & 0x3f));
        }
      }

      // Add padding
      var paddingChar = map.charAt(64);
      if (paddingChar) {
        while (base64Chars.length % 4) {
          base64Chars.push(paddingChar);
        }
      }

      return base64Chars.join("");
    },

    /**
     * Converts a Base64 string to a word array.
     *
     * @param {string} base64Str The Base64 string.
     *
     * @return {WordArray} The word array.
     *
     * @static
     *
     * @example
     *
     *     var wordArray = CryptoJS.enc.Base64.parse(base64String);
     */
    parse: function(base64Str) {
      // Shortcuts
      var base64StrLength = base64Str.length;
      var map = this._map;

      // Ignore padding
      var paddingChar = map.charAt(64);
      if (paddingChar) {
        var paddingIndex = base64Str.indexOf(paddingChar);
        if (paddingIndex !== -1) {
          base64StrLength = paddingIndex;
        }
      }

      // Convert
      var words = [];
      var nBytes = 0;
      for (var i = 0; i < base64StrLength; i++) {
        if (i % 4) {
          var bits1 = map.indexOf(base64Str.charAt(i - 1)) << ((i % 4) * 2);
          var bits2 = map.indexOf(base64Str.charAt(i)) >>> (6 - (i % 4) * 2);
          words[nBytes >>> 2] |= (bits1 | bits2) << (24 - (nBytes % 4) * 8);
          nBytes++;
        }
      }

      return WordArray.create(words, nBytes);
    },

    _map: "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/="
  });
  // console.log(Base64);
})();



videojs.registerPlugin('relatedVideosRecommendation', function (options) {
  // console.log("relatedVideosRecommendation Plugin is registered to Player.");
  const AMPERSAND = "&";
  const MERCHANT_ID = options.merchantId;
  const WRUAPI = `https://${options.host}/${MERCHANT_ID}`;
  const WRU = "wru";
  var user_tracker_ID = "333abc",
    event_id = "4001",
    deviceIP = "1.1.1.1",
    recommendationsRequestOptions = {},
    videoData;
  var myPlayer;
  var recommendedVideos = [];
  var recommendationAPIResponse = [];
  var playerElem = $(document.getElementById(this.id()));

  this.ready(function () {

    myPlayer = this;

    var share = document.getElementById(myPlayer.id()).getElementsByClassName('vjs-dock-shelf')[0];

    // remove the text at top
    var docktext = document.getElementById(myPlayer.id()).getElementsByClassName('vjs-dock-text')[0];
    docktext.parentNode.removeChild(docktext);

    // insert the share before quality menu button
    var controlBar = document.getElementById(myPlayer.id()).getElementsByClassName('vjs-control-bar')[0];
    insertBeforeNode = document.getElementById(myPlayer.id()).getElementsByClassName('vjs-playback-rate')[0];

    share.className = 'vjs-control'; // update the class name for styling
    // Insert the share div in proper location
    controlBar.insertBefore(share, insertBeforeNode);

  });


  this.on("loadstart", function () {
    // remove the share span text
    document.getElementById(this.id()).getElementsByClassName('vjs-share-control')[0].getElementsByTagName('span')[1].innerHTML = '';

    playerElem.find('.vjs-share-control').on('click', function () {
      // console.log("Entered Share Click  function");
      playerElem.find('.pauseOverlay').addClass("vjs-hidden");
      playerElem.find('.end-fivesec-overlay').addClass("vjs-hidden");
      
      // console.log("hidden class added");
      
    });
  });


  this.on('loadedmetadata', function () {
    try {
      greet()
    } catch (e) {
      console.log("Error occurred while fetching external library.");
    }
    myPlayer = this;
    // console.log(`metadata of video : ${myPlayer.mediainfo.id} is loaded.`);

    setUserTrackerID();
    setRequestDataForRecommendations();
    makeRequestForVideoRecommendations(recommendationsRequestOptions, processRecommendedVideoData, myPlayer.id());
 
 
    this.on('pause',function(){
      if(myPlayer.currentTime()>=(myPlayer.duration()-5))
        playerElem.find('.end-fivesec-overlay').addClass('vjs-hidden');
  });

    this.on('play',function(){

         if(myPlayer.currentTime()>=(myPlayer.duration()-5))
          playerElem.find('.end-fivesec-overlay').removeClass('vjs-hidden');

    });



    this.on('ended', function () {
      // console.log("Video has ended!!")
      playerElem.find('.pauseOverlay').addClass("vjs-hidden");
      playerElem.find('.end-fivesec-overlay').addClass("vjs-hidden");
      
      playerElem.find('.endOverlay .ytp-upnext').show();
      playerElem.find('.endOverlay .carousel').hide();

      playerElem.find('.ytp-upnext-cancel').on('click', function () {
        clearTimeout(playNext);
        playerElem.find('.endOverlay .carousel').slick('unslick');
        setSlickCarouselProp();
        playerElem.find('.endOverlay .carousel').show();
        playerElem.find('.endOverlay .ytp-upnext').hide();
        verticalImageFit();


      });

      playerElem.find('.ytp-upnext-autoplay-icon').on('click', function () {
        clearTimeout(playNext);
        playNextVideo();
      });

      var playNextVideo = function () {
        // console.log("Play next function entered !!");
        playerElem.find('.endOverlay .ytp-upnext').hide();
       
        myPlayer.catalog.getVideo(recommendedVideos[0], function (error, video) {
          if (error === null) {
            // Load the video object into the player
            myPlayer.catalog.load(video);
            // Play the video
            myPlayer.play();
            // Remove event listener or will be in infinite loop playing last video
            myPlayer.off('ended');
          }
        })
      };

      var playNext = setTimeout(function () {
        playNextVideo();
      }, 5000);
    })
  });

  this.on('fullscreenchange', elementResize);
  //  Set user_tracker_ID from cookies else from URL

  function setUserTrackerID() {
    var found_ID = false;
    var cookies = document.cookie.split("; ");
    cookies.forEach(cookie => {
      var searchPattern = new RegExp("^" + "thinmint");
      if (searchPattern.test(cookie)) {
        // console.log("Device tracker Id :", cookie);
        user_tracker_ID = cookie.split("=")[1];
        found_ID = true;
      }
    });

    if (!found_ID) {
      var argv = document.URL.split("&");
      argv.forEach(arg => {
        var searchPattern = new RegExp("^" + "id");
        if (searchPattern.test(arg)) {
          // console.log("Device tracker Id :", arg);
          user_tracker_ID = arg.split("=")[1];
          found_ID = true;
        }
      });
    }

    setRequestDataForUserEvent("VIDEO_VIEW_EVENT", "wru", myPlayer.mediainfo.id);
  }

  /**
   *Sets up data for user_event request for the DSS API
   */
  var setRequestDataForUserEvent = function (eventType, recommendation, cid) {
    var time = String((new Date()).getTime());
    
  
    var userEventRequestFormat = {
      "cid": cid,
      "uid": user_tracker_ID,
      "merchantId": MERCHANT_ID,
      "eid": event_id,
      "ref": WRU,
      "timestamp": time,
      "eventType": eventType,
      "recommendation": recommendation
    };
    // console.log("Setting data for user event request.");
    // console.log("WRU API: "+WRUAPI);
    // console.log(userEventRequestFormat);
    sendUserEventRequest(userEventRequestFormat);
  };

  /**
   *POSTs user_event to DSS API
   */
  var sendUserEventRequest = function (userEventRequestData) {
    // console.log("Sending user event request.");
    var httpRequest = new XMLHttpRequest();
    var dataToSend = [userEventRequestData];
    handleResponse = function () {
      try {
        if (httpRequest.readyState === 4) {
          if (httpRequest.status >= 200 && httpRequest.status < 300) {
            response = httpRequest.responseText;
            if (response === "{null}") {
              response = null;
            }
            // console.log(response);
          } else {
            console.log("There was a problem with the request. Request returned " + httpRequest.status);
            // alert(
            //   "There was a problem with the request. Request returned " +
            //   httpRequest.status
            // );
          }
        }
      } catch (e) {
        console.log("Caught Exception: " + e);
        //alert("Caught Exception: " + e);
      }
    };
    // console.log("Hitting", WRUAPI + "/user", "to post user event.");
    httpRequest.onreadystatechange = handleResponse;
    httpRequest.open("POST", WRUAPI + "/user", true);
    httpRequest.send(JSON.stringify(dataToSend));
  };

  /**
   *creates Overlay Content in HTML, from response data received from recommendations API
   */
  var processRecommendedVideoData = function (recommendedVideos, playerId) {
    if (recommendedVideos) {
      // console.log("response from API in jsonString Format : \n", recommendedVideos);
      var JSONrecommendedVideos = JSON.parse(recommendedVideos);

      if (JSONrecommendedVideos[0]["recommendations"] !== undefined) {
        videoData = JSONrecommendedVideos[0]["recommendations"];
      }
      videoData = extractVideoData(videoData);

      // console.log("Extracted Video data is : \n", videoData);
      // console.log("adding Overlay  to video.");

      addOverlay(videoData, playerId);
       
      // console.log("added overlay to video player.");
    }
  };

  /**
   * Sets up data for user_event request for the DSS API
   */
  function setRequestDataForRecommendations() {
    // console.log("Setting request data for recommendations.");
    const endPoint = "st=vsims&source=true";
    // console.log("current media info : \n", myPlayer.mediainfo);

    recommendationsRequestOptions.videoName = myPlayer.mediainfo.name;
    recommendationsRequestOptions.articleId = myPlayer.mediainfo.id;
    recommendationsRequestOptions.uid = user_tracker_ID;
    recommendationsRequestOptions.deviceIP = deviceIP;

    recommendationsRequestOptions.requestType = "GET";
    recommendationsRequestOptions.url = WRUAPI + "?" + "uid=" + recommendationsRequestOptions.uid + AMPERSAND + "vid=" + recommendationsRequestOptions.articleId + AMPERSAND + endPoint;

  }

  /**
   * send API request to the proxy
   * @param  {Object} options for the request
   * @param  {String} options.url the full API request URL
   * @param  {String="GET","POST","PATCH","PUT","DELETE"} requestData [options.requestType="GET"] HTTP type for the request
   * @param  {String} options.proxyURL proxyURL to send the request to
   * @param  {String} options.client_id client id for the account (default is in the proxy)
   * @param  {String} options.client_secret client secret for the account (default is in the proxy)
   * @param  {JSON} [options.requestBody] Data to be sent in the request body in the form of a JSON string
   * @param  {Function} [callback] callback function that will process the response
   */
  function makeRequestForVideoRecommendations(options, callback, playerId) {
    var httpRequest = new XMLHttpRequest(),
      response;
    const recommendationAPI_URL = options.url;
    var secret = "keygen",content_type =  "application/json";
    let result = generateSignature(recommendationAPI_URL, secret, content_type);
    // var videoRequestFormat = {
    //   "Authorization": "Authorization me:" + result[0],
    //   "DateUsed": result[1]
    // }
    // console.log("videoRequestFormat:  "+videoRequestFormat);
    getResponse = function () {
      try {
        if (httpRequest.readyState === 4) {
          if (httpRequest.status >= 200 && httpRequest.status < 300) {
            response = httpRequest.responseText;
            if (response === "{null}") {
              response = null;
            }
            recommendationAPIResponse = response;
            callback(response, playerId);
          } else {
            console.log("There was a problem with the request. Request returned " + httpRequest.status);

            // alert(
            //   "There was a problem with the request. Request returned " +
            //   httpRequest.status
            // );
          }
        }
      } catch (e) {

        console.log("Caught Exception: " + e);
        // alert("Caught Exception: " + e);
      }
    };

    // console.log("Hitting", recommendationAPI_URL, "for video recommendations.");
    httpRequest.onreadystatechange = getResponse;
    httpRequest.open("GET", recommendationAPI_URL);
    httpRequest.setRequestHeader("Content-Type", content_type);
    httpRequest.setRequestHeader("Accept", "application/json");
    httpRequest.setRequestHeader("Authorization", "Authorization me:" + result[0]);
    httpRequest.setRequestHeader("DateUsed", result[1]);

    // console.log("JSON Stringify: "+JSON.stringify(videoRequestFormat));
    httpRequest.send();

  }

  /**
   * extract video datacreateSinglesponse
   * @param {array} reccreateSinglata the data from the CMS API
   * @return {array} videoData array of video info
   */
  function extractVideoData(recommendedVideosData) {
    recommendedVideos = []; // re-init to empty array
    extractedVideoData = [];
    iMax = recommendedVideosData.length > 18 ? 18 : recommendedVideosData.length;
    for (var i = 0; i < iMax; i++) {
      if (recommendedVideosData[i].Cid !== null && recommendedVideosData[i].Thumbnail !== undefined) {
        let videoItem = {};
        recommendedVideos.push(recommendedVideosData[i].Cid);
        videoItem.id = recommendedVideosData[i].Cid;
        videoItem.name = recommendedVideosData[i].Description;
        videoItem.simsScore = recommendedVideosData[i].SimsScore;
        videoItem.thumbnail = recommendedVideosData[i].Thumbnail.replace("160x90", "400x255");
        videoItem.title = recommendedVideosData[i].Description

        extractedVideoData.push(videoItem);
      }
    }
    return extractedVideoData;
  }

  /**
   * intializes the overlay plugin with the related video thumbnails
   * @param {HTML} overlayContent the HTML for the overlay
   */
  function addOverlay(videoData, playerId) {
    var pauseOverlayContent = createSingleRowCarousel(videoData, playerId);
    var endOverlayContent = createMultiRowCarousel(videoData, playerId);
    var upNextVideoTitle = videoData.length > 0 && videoData[0].title ? videoData[0].title : '';
    var upNextThumbnail = videoData.length > 0 && videoData[0].thumbnail ? videoData[0].thumbnail : '';
    var currVideoDuration = myPlayer.duration();
    var upnextVideoRef = 'javascript:loadAndPlay("' + videoData[0].id + '",' + '"' + playerId + '"' + ')';
    var closeOverlay = "playerElem.find('.end-fivesec-overlay').addClass('vjs-hidden')";
    
    // console.log("Video duration:" + currVideoDuration);
    myPlayer.overlay({
      attachToControlBar: false,
      overlays: [{
        content: pauseOverlayContent,
        start: 'pause',
        end: 'play',
        align: 'bottom',
        class: 'pauseOverlay'
      },
      {
        start: (currVideoDuration - 5),
        end: currVideoDuration,
        align: 'bottom-left',
        content: '<a href ='+upnextVideoRef+'>'+
                 '<div class="upnext-overlay-item upnext-text-box" style="width:45%"><span style="border:1px solid white;padding:5%;display:inline-block">Up Next</span></div>' +
                 '<img class="upnext-overlay-item" src="' + upNextThumbnail + '" style="width:50%;height:90%;position:relative;bottom:47%;object-fit:contain;background-color:rgb(0,0,0,0.5)">' +
                 '</a>',
                 showBackground: false,
        class: 'end-fivesec-overlay'
      },


      {
        start: 'ended',
        end: 'play',
        align: 'bottom-left',
        content: '<div class="ytp-upnext ytp-player-content ytp-upnext-autoplay-paused" style="/* display: none; */" data-layer="4">' +
          '<div class="ytp-upnext-container"><span class="ytp-upnext-top"><span class="ytp-upnext-header">Up Next</span><span class="ytp-upnext-title">' + upNextVideoTitle + '</span></span>' +
          '<a class="ytp-upnext-autoplay-icon" role="button" aria-label="Play next video" style="display: block;cursor:pointer;">' +
          '<svg height="100%" version="1.1" viewBox="0 0 72 72" width="100%">' +
          '<circle class="ytp-svg-autoplay-circle" cx="36" cy="36" fill="#fff" fill-opacity="0.3" r="31.5"></circle>' +
          '<circle class="ytp-svg-autoplay-ring" cx="-36" cy="36" fill-opacity="0" r="33.5" stroke="#FFFFFF" stroke-dasharray="211" stroke-dashoffset="-211.00012878417968" stroke-width="4" transform="rotate(-90)"></circle>' +
          '<path class="ytp-svg-fill" d="M 24,48 41,36 24,24 V 48 z M 44,24 v 24 h 4 V 24 h -4 z"></path>' +
          '</svg>' +
          '</a>' +
          '<span class="ytp-upnext-bottom"><span class="ytp-upnext-cancel"><button class="ytp-upnext-cancel-button ytp-button" tabindex="0" aria-label="Cancel autoplay">Cancel</button></span></span></div>' +
          '</div>' + endOverlayContent.outerHTML,
        class: 'endOverlay'
      }
      ]
    });
    setJCarouselProp();
    setSlickCarouselProp();
    playerElem.find('.vjs-overlay-bottom').css({ 'margin-left': '0', 'bottom': '30px' });
    playerElem.find('.vjs-overlay-bottom').addClass('pull-left');
    
  }

  /**
   * create the html for the overlay
   * @param {array} videoData array of video objects
   * @return {HTMLElement} videoList the div element containing the overlay
   */
  loadAndPlay = function (idx, playerId) {
    // console.log(videoData + "/" + playerId);
    setRequestDataForUserEvent("VIDEO_RECOMMENDATION_VIEW_EVENT", "wru", idx);
    var currentId = idx;
    var player = bc(playerId);
    player.catalog.getVideo(currentId, function (error, video) {
      try {
        player.catalog.load(video);
      } catch (e) {
        player.catalog.load(video);
      }
      player.play();
    });
  }

  //To be dealt by UX Dev
  function createSingleRowCarousel(videoData, playerId) {

    // console.log("createvideodata: " + videoData);
    var i,
      iMax = videoData.length,
      rows = createEl('div', {
        class: 'wrapper'
      }),
      cols = createEl('div', {
        class: 'jcarousel-wrapper'
      }),
      carousel_outer = createEl('div', {
        class: 'jcarousel'
      });
    carousel_outer.setAttribute("data-jcarousel", "true");
    var carousel_inner = document.createElement("ul"),

      item_class,
      item_class1,
      item_cols,
      thumbnailLink,
      thumbnailText,
      thumbnailImage;
    rows.appendChild(cols);
    cols.appendChild(carousel_outer);
    carousel_outer.appendChild(carousel_inner);


    for (i = 0; i < iMax; i++) {
      // console.log("inside the imax loops");

      item_class = document.createElement("li");


      thumbnailLink = createEl('a', {
        href: 'javascript:loadAndPlay("' + videoData[i].id + '",' + '"' + playerId + '"' + ')'
      })
      thumbnailLink.style.textDecoration = "none";


      thumbnailImage = createEl('img', {
        class: 'img-responsive',
        src: videoData[i].thumbnail,
        alt: videoData[i].title,
        onError: '$(this).parent().parent().remove();'
      });
      thumbnailImage.style.height = "90px";
      thumbnailImage.style.width = "200px";
      thumbnailImage.style['object-fit'] = "contain";
      thumbnailText = createEl('div', {
        class: 'text-block'
      });


      carousel_inner.appendChild(item_class);
      item_class.appendChild(thumbnailLink);
      thumbnailLink.appendChild(thumbnailImage);
    }


    var slider_arrow_left = createEl('a', {
      href: '#',
      class: 'jcarousel-control-prev'

    });



    slider_arrow_left.setAttribute("data-jcarouselcontrol", "true");

    //slider_arrow_left.innerHTML = "ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã‚Â¦ ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â²";

    slider_arrow_left.style.fontSize = "20px";

    var il = createEl('i', {
      class: 'left'
    });

    slider_arrow_left.appendChild(il);


    var slider_arrow_right = createEl('a', {
      href: '#',
      class: 'jcarousel-control-next'

    });
    slider_arrow_right.setAttribute("data-jcarouselcontrol", "true");

    //slider_arrow_right.innerHTML = "ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã‚Â¦ ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â³";
    slider_arrow_right.style.fontSize = "20px";

    var ir = createEl('i', {
      class: 'right'
    });

    slider_arrow_right.appendChild(ir);

    cols.appendChild(slider_arrow_left);
    cols.appendChild(slider_arrow_right);

    // console.log("after list creation: ", rows);
    return rows;
  }


  function createEl(type, attributes) {
    var el,
      attr;
    el = document.createElement(type);
    if (attributes !== null) {
      for (attr in attributes) {
        el.setAttribute(attr, attributes[attr]);
      }
      return el;
    }
  }



  // Multi Row Carousel
  var setSlickCarouselProp = function () {
    playerElem.find('.carousel').slick({
      dots: true,
      slidesPerRow: 3,
      rows: 3,
      arrows: true,
      infinite: true,
      prevArrow: '<a href="#" class="carousel-control-prev"><i class="left"></i></a>',
      nextArrow: '<a href="#" class="carousel-control-next"><i class="right"></i></a>',

      responsive: [
        {
          breakpoint: 770,
          settings: {
            slidesPerRow: 2,
            rows: 2,
          }
        },
        {
          breakpoint: 575,
          settings: {
            slidesPerRow: 1,
            rows: 1,
          }
        }
      ]
    });

    playerElem.find('.carousel-elem').parent().addClass("carouselrow-overlay-ratio");
    playerElem.find(".carousel").on('breakpoint', function (event, slick, breakpoint) {
      // console.log('breakpoint ' + breakpoint);
      playerElem.find('.carousel-elem').parent().addClass("carouselrow-overlay-ratio");
    });
  };


  var createMultiRowCarousel = function (videoData, playerId) {
    // console.log("creating elements for", videoData);
    var carousel = createEl('div', {
      class: 'carousel'
    });

    for (i = 0; i < videoData.length; i++) {
      var carouselElem = createEl('div', { class: 'carousel-elem' });

      var thumbnailLink = createEl('a', {
        class: 'image-anch',
        href: 'javascript:loadAndPlay("' + videoData[i].id + '",' + '"' + playerId + '"' + ')'
      })
      thumbnailLink.style.textDecoration = "none";

      var thumbnailImage = createEl('img', {
        class: 'img-responsive',
        src: videoData[i].thumbnail,
        alt: videoData[i].title,
        onError: "$(this).parent().parent().remove();"
      });
      thumbnailImage.style.background = "black";
      // console.log("Image Validity Check");

      carousel.appendChild(carouselElem);
      carouselElem.appendChild(thumbnailLink);
      thumbnailLink.appendChild(thumbnailImage);
    }
    // console.log("\nVideo Elements created : ", carousel);

    return carousel;
  };


  var setJCarouselProp = function () {
    var jcarousel = playerElem.find('.jcarousel');

    jcarousel
      .on('jcarousel:reload jcarousel:create', function () {
        var carousel = $(this),
          width = carousel.innerWidth();
      })
      .jcarousel({
        wrap: 'circular',
        scroll: 3,
        visible: 3,
        rows: 2
      });

    playerElem.find('.jcarousel-control-prev')
      .jcarouselControl({
        target: '-=1'
      });

    playerElem.find('.jcarousel-control-next')
      .jcarouselControl({
        target: '+=1'
      });
  }
  var verticalImageFit = function () {
    // console.log("vertical Image function entered !!");
    playerElem.find('.image-anch img').each(function () {
      // console.log("iterating each images !! Height: " + this.height);

      if (this.naturalHeight > this.naturalWidth) {
        // console.log("Height and width compared!!");

        this.style.objectFit = 'contain';

        // console.log("Class added!!");

      }
      else {
        this.style.objectFit = 'cover';
      }


    });



  }

  

});



var elementResize = function () {
  // console.log('trigger ressize');
  if (window.innerWidth < 576) {
    if ($('.video-js').hasClass('vjs-fullscreen')) {

      $('.carousel-elem').addClass('internal-elem');
    }
    else {
      $('.carousel-elem').removeClass('internal-elem');
    }
  }
  else {
    $('.carousel-elem').removeClass('internal-elem');
  }
};

window.onresize = elementResize; 

